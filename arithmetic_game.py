import random
import time

from datetime import timedelta
from operator import attrgetter


class User(object):
    
    def __init__(self, username='', time='', errors=''):
        """
        Stores the values of a user's username, time, and errors

        Args:
        username(String): The user's name
        time(String): The time it took for the user to finish the game
        errors(String): Total number of a user's wrong answers
        """
        self.username = username
        self.time = time
        self.errors = errors


def get_users(filename):
    """
    Collects data from the user records txt file,
    stores all user's name, time, and errors in
    a class and returns a list of user class.

    Args:
    filename(String): The .txt file's name

    Returns:
    user_list(User): A list of User classes assigned with
    specific attribute values from the file
    """
    user_file = open(filename, 'r')
    user_list = []
    
    for line in user_file:
        data_per_tab = line.strip('\n').split('\t')
        user = User(data_per_tab[0], data_per_tab[1], data_per_tab[2])
        user_list.append(user)

    user_file.close()
    return user_list


def set_users(filename, user_list):
    """
    Writes the name, time, and errors of all users
    stored inside the list into the file.

    Args:
    filename(String): The .txt file's name
    user_list(User): A list of User classes with
    specific attribute values assigned
    """
    user_file = open(filename, 'w')
    
    for index in xrange(len(user_list)):
        user_name = user_list[index].username
        user_time = user_list[index].time
        user_errors = str(user_list[index].errors)
        user_file.writelines(user_name + '\t' + user_time + '\t' + user_errors + '\n')

    user_file.close()


def get_question_and_answer():
    """
    Picks a random operation and returns a dictionary that
    contains a random equation along with it's answer.

    Returns:
    ({'question': String, 'answer': Integer}): Dictionary of an equation
    and the answer
    """
    operation = random.randint(0, 3)    # Gets random integer to pick from 4 operations.
    
    if operation is 0:
        first_number = random.randint(1, 99)
        second_number = random.randint(1, 99)
        question = str(first_number) + ' + ' + str(second_number)
        answer = first_number + second_number
        return {'question': question, 'answer': answer}
    elif operation is 1:
        first_number = random.randint(1, 99)
        second_number = random.randint(1, 99)

        if second_number > first_number:
            temporary_number = second_number
            second_number = first_number
            first_number = temporary_number
        
        question = str(first_number) + ' - ' + str(second_number)
        answer = first_number - second_number
        return {'question': question, 'answer': answer}
    elif operation is 2:
        first_number = random.randint(2, 99)
        second_number = random.randint(2, 9)
        question = str(first_number) + ' * ' + str(second_number)
        answer = first_number * second_number
        return {'question': question, 'answer': answer}
    elif operation is 3:
        first_number = random.randint(2, 999)
        second_number = random.randint(2, 9)

        if first_number%second_number:

            if second_number > first_number:
                temporary_number = second_number
                second_number = first_number
                first_number = temporary_number

            first_number -= first_number%second_number
    
        question = str(first_number) + ' / ' + str(second_number)
        answer = first_number / second_number
        return {'question': question, 'answer': answer}


def set_points(user_answer, right_answer, points):
    """
    Tries if the user's input is a number or not displays
    if the user's answer is right or wrong and returns
    an updated dictionary of right and wrong points
    depending if the user is right or wrong.

    Args:
    user_answer(String): The user's input as an answer for the equation
    right_answer(Integer): The correct answer for the equation
    points({'right': Integer, 'wrong': Integer}): Dictionary of right
    and wrong points of the current user

    Returns:
    ({'right': Integer, 'wrong': Integer}): Updated dictionary of right
    and wrong points of the current user
    """
    try:
        user_answer = int(user_answer)
    except Exception,  e:
        print 'You are wrong! The correct answer is ' + str(right_answer)
        return {'right': points['right'], 'wrong': points['wrong']+1}
    else:
        if user_answer == right_answer:
            print 'You are right! Good job!'
            return {'right': points['right']+1, 'wrong': points['wrong']}
        else:
            print 'You are wrong! The correct answer is ' + str(right_answer)
            return {'right': points['right'], 'wrong': points['wrong']+1}


def get_time(time_began):
    """
    Returns how long it took for the user
    to finish the game in a time format.

    Args:
    time_began(Float): The time recorded when the user started the game

    Returns:
    (String): timedelta format of the time it took for the
    user to finish the game
    """
    time_score = int(time.time())-int(time_began)
    return str(timedelta(seconds = time_score))


def get_is_updated(username, new_time, points):
    """
    Updates a user's score if it's a new record and returns
    a boolean value if a user's score is updated.

    Args:
    username(String): The user's name
    new_time(String): The time it took for the current user
    to finish the game
    points({'right': Integer, 'wrong': Integer}): Dictionary of right
    and wrong points of the current user

    Returns:
    (Boolean): True if the user's score is updated and False otherwise
    """
    global user_list
    
    for index in xrange(len(user_list)):
        if username == user_list[index].username:
            old_time = user_list[index].time

            if new_time<old_time or (new_time==old_time and points['wrong']<user_list[index].errors):
                user_list[index].username = username
                user_list[index].time = new_time
                user_list[index].errors = points['wrong']
            
            return True
    
    return False


def set_score(username, time_score, points):
    """
    Displays the user's final score and sets/records it in the user list.

    Args:
    username(String): The user's name
    time_score(String): The time it took for the current user
    to finish the game
    points({'right': Integer, 'wrong': Integer}): Dictionary of right
    and wrong points of the current user
    """
    global user_list
    global get_is_updated
    print 'You have completed the challenge!'
    print 'Time: ' + time_score + '    Errors: ' + str(points['wrong'])
    is_updated = get_is_updated(username, time_score, points)               # Checks if the user's score is updated.

    if not is_updated:
        user = User(username, time_score, points['wrong'])
        user_list.append(user)


def get_sorted_user_list(user_list):
    """
    Sorts the list of users based on time and then errors

    Args:
    user_list(User): A list of User classes assigned with
    specific attribute values

    Returns:
    sorted_user_list(User): List of sorted User classes
    based on time and then errors
    """
    sorted_user_list = sorted(user_list, key = attrgetter('time', 'errors'))
    
    for index in xrange(len(sorted_user_list)):

        if index is 10:         # Breaks after displaying the top 10 scores.
            break
        
        user_name = sorted_user_list[index].username
        user_time = sorted_user_list[index].time
        user_errors = str(sorted_user_list[index].errors)
        print str(index + 1) + '.) ' + user_name + ' ' + user_time + ' ' + user_errors
    
    return sorted_user_list


# The program starts here.
user_list = get_users('users.txt')
username = raw_input('Enter username: ')

while username:
    is_start = raw_input('Press enter key to start')      # Pauses the program until the user presses enter.
    time_began = time.time()                            # Records starting time.
    points = {'right': 0, 'wrong': 0}

    while points['right']<10 and points['wrong']<10:
        question_and_answer = get_question_and_answer()
        question = question_and_answer['question']
        answer = question_and_answer['answer']
        points = set_points(raw_input('What is ' + question + '? '), answer, points)

    time_score = get_time(time_began)

    if points['right'] == 10:
        set_score(username, time_score, points)
    
    if points['wrong'] == 10:
        print 'You have failed the challenge!'
        print 'Time: ' + time_score + '    Errors: ' + str(points['wrong'])
    
    username = raw_input('Enter username: ')

sorted_user_list = get_sorted_user_list(user_list)
set_users('users.txt', sorted_user_list)